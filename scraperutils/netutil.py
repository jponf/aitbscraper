#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib.request
import requests

_PUBLIC_IP_QUERY = 'https://api.ipify.org?format=json'


def get_pulic_ip():
    """Queries an external server for the public IP of this computer.
    :return: The public IP of this computer
    """
    r = requests.get(_PUBLIC_IP_QUERY)
    r.raise_for_status()
    return r.json()['ip']


def escape_url(url, ignore=''):
    """Escapes invalid URL characters.

    :param url: The url to scape.
    :param ignore: Characters that must be preserved as they are.
    :return: The scaped url.
    """
    return urllib.request.quote(url, safe=':/?=' + ignore)
