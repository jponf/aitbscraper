# -*- coding: utf-8 -*-

import collections
import logging
import lxml.html
import os.path
import requests
import sqlite3

# ------------
#     DAOs
# ------------

BookInfo = collections.namedtuple(
    'BookInfo',
    ['title', 'authors', 'isbn', 'year', 'pages', 'language',
     'category', 'description', 'file_name', 'file_format']
)

# ------------------
#     Exceptions
# ------------------

class RequestError(Exception):
    """"""


# ------------------------------------
#     Module Constants & Variables
# ------------------------------------

BASE_INDEX_URL = 'http://www.allitebooks.com/page'
BOOKS_PER_PAGE = 10
BOOKS_EXTENSIONS = ('.pdf', '.rar', '.zip', '.epub')

_XP_AUTHOR_DD = '//dt[text()="Author:"]//following-sibling::dd[1]'
_XP_ISBN_DD = '//dt[text()="Isbn:"]//following-sibling::dd[1]'
_XP_YEAR_DD = '//dt[text()="Year:"]//following-sibling::dd[1]'
_XP_PAGES_DD = '//dt[text()="Pages:"]//following-sibling::dd[1]'
_XP_LANG_DD = '//dt[text()="Language:"]//following-sibling::dd[1]'
_XP_FSIZE_DD = '//dt[text()="File size:"]//following-sibling::dd[1]'
_XP_FFRMT_DD = '//dt[text()="File format:"]//following-sibling::dd[1]'
_XP_CATEG_DD = '//dt[text()="Category:"]//following-sibling::dd[1]'

_XP_BOOK_TITLE = '//h1[@class="single-title"]/text()'
_XP_BOOK_DESC = '//div[@class="entry-content"]/child::*'

_logger = logging.getLogger(__name__)


# --------------------
#     AITB Scraper
# --------------------

class AITBScraper(object):

    def __init__(self, max_retries):
        self._max_retries = max_retries
        self._visited = set()

    def get_index_page_book_urls(self, index):
        url = "{0}/{1}".format(BASE_INDEX_URL, index)
        r = self._perform_request(url)
        tree = lxml.html.fromstring(r.content)

        # Extract href and remove duplicates
        hrefs = list(set(tree.xpath('//a[@rel="bookmark"]/@href')))

        if len(hrefs) != BOOKS_PER_PAGE:
            _logger.warn("Found %d book references, expected %d",
                         len(hrefs), BOOKS_PER_PAGE)

        return hrefs

    def get_book_info(self, book_url):
        r = self._perform_request(book_url)
        tree = lxml.html.fromstring(r.content)

        download_url = _extract_book_download_url(tree)
        info = _generate_book_info(tree, download_url)

        return (info, download_url)

    def download_book(self, url):
        r = self._perform_request(url)
        return r.content

    def add_visited(self, url):
        self._visited.add(url)

    def visited(self, url):
        return url in self._visited

    def _perform_request(self, url):  # requests the given max_retries times
        r = requests.get(url)
        tries = 1
        while r.status_code != requests.codes.ok:
            if tries > self._max_retries:
                raise RequestError("Unable to get resource '%s' after %d tries"
                                   % (url, tries))
            r = requests.get(url)
            tries += 1

        return r


# ---------------------
#     AITB DataBase
# ---------------------

_DB_TABLE_CATEG = 'categories'
_DB_TABLE_FFRMT = 'fileformats'
_DB_TABLE_LANGS = 'languages'
_DB_TABLE_BOOKS = 'books'
_DB_TABLE_AUTHORS = 'authors'


class AITBDataBase(object):

    def __init__(self, db_path):
        """Creates a new database object that manages an underlying sqlite3 db

        :param db_path: Path to the database.
        """
        self._con = sqlite3.connect(db_path)

        self._create_tables()


    def insert_book(self, book_info):
        """Performs the necessary operations to insert the given BookInfo.

        :param book_info: Book information to insert into the database.
        :type book_info: BookInfo
        """
        c = self._con.cursor()
        # TODO properly check if value exists
        c.execute('''INSERT OR IGNORE INTO {0} (category) VALUES ("{1}");
                  '''.format(_DB_TABLE_CATEG, book_info.category))

        c.execute('''INSERT OR IGNORE INTO {0} (format) VALUES ("{1}");
                  '''.format(_DB_TABLE_FFRMT, book_info.file_format))

        c.execute('''INSERT OR IGNORE INTO {0} (language) VALUES ("{1}");
                  '''.format(_DB_TABLE_LANGS, book_info.language))

        c.execute('''INSERT INTO {0}
                     (title, isbn, description, category, file, format, language)
                     VALUES ("{1}", "{2}", "{3}", "{4}", "{5}", "{6}", "{7}")
                  '''.format(_DB_TABLE_BOOKS, book_info.title, book_info.isbn,
                             book_info.description, book_info.category,
                             book_info.file_name, book_info.file_format,
                             book_info.language))
        self._con.commit()

    def _create_tables(self):
        c = self._con.cursor()
        if not self._table_exists(_DB_TABLE_CATEG):
            _logger.info("Creating table '%s'" % _DB_TABLE_CATEG)
            c.execute('''CREATE TABLE {0} (
                            category VARCHAR(255) NOT NULL PRIMARY KEY
                         );'''.format(_DB_TABLE_CATEG))

        if not self._table_exists(_DB_TABLE_FFRMT):
            _logger.info("Creating table '%s'" % _DB_TABLE_FFRMT)
            c.execute('''CREATE TABLE {0} (
                            format VARCHAR(255) NOT NULL PRIMARY KEY
                         );'''.format(_DB_TABLE_FFRMT))

        if not self._table_exists(_DB_TABLE_LANGS):
            _logger.info("Creating table '%s'" % _DB_TABLE_LANGS)
            c.execute('''CREATE TABLE {0} (
                            language VARCHAR(255) NOT NULL PRIMARY KEY
                         );'''.format(_DB_TABLE_LANGS))

        if not self._table_exists(_DB_TABLE_BOOKS):
            _logger.info("Creating table '%s'" % _DB_TABLE_BOOKS)
            c.execute('''CREATE TABLE {0} (
                            book_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            title TEXT NOT NULL,
                            isbn VARCHAR(20),
                            description TEXT,
                            file TEXT,
                            category VARCHAR(255),
                            format VARCHAR(255),
                            language VARCHAR(255),
                            FOREIGN KEY (category) REFERENCES {1}(category),
                            FOREIGN KEY (format) REFERENCES {2}(format),
                            FOREIGN KEY (language) REFERENCES {3}(language)
                         );'''.format(_DB_TABLE_BOOKS, _DB_TABLE_CATEG,
                                      _DB_TABLE_FFRMT, _DB_TABLE_LANGS))

        self._con.commit()

    def _table_exists(self, table_name):
        cur = self._con.execute(
                "SELECT count(*) FROM sqlite_master WHERE type = 'table' "
                "AND name = '%s'" % table_name)

        row = cur.fetchone()
        assert row is not None
        return row[0] > 0


# ------------------------
#     Utiliy Functions
# ------------------------

def _generate_book_info(tree, download_url):
    title = tree.xpath(_XP_BOOK_TITLE)
    authordd = tree.xpath(_XP_AUTHOR_DD)
    isbndd = tree.xpath(_XP_ISBN_DD)
    yeardd = tree.xpath(_XP_YEAR_DD)
    pagesdd = tree.xpath(_XP_PAGES_DD)
    langdd = tree.xpath(_XP_LANG_DD)
    # fsizedd = tree.xpath(_XP_FSIZE_DD)
    ffrmtdd = tree.xpath(_XP_FFRMT_DD)
    categdd = tree.xpath(_XP_CATEG_DD)

    desc = _process_book_description(tree.xpath(_XP_BOOK_DESC))
    authors_str = authordd[0].text_content() if authordd else ""

    return BookInfo(
        title=title[0] if title else "<no-title>",
        authors=[s.strip() for s in authors_str.split(',')],
        isbn=isbndd[0].text_content().strip() if isbndd else "",
        year=yeardd[0].text_content().strip() if yeardd else "",
        pages=pagesdd[0].text_content().strip() if pagesdd else "",
        language=langdd[0].text_content().strip() if langdd else "",
        category=categdd[0].text_content().strip() if categdd else "",
        description=desc,
        file_name=os.path.basename(download_url),
        file_format=ffrmtdd[0].text_content().strip() if ffrmtdd else "")


def _process_book_description(parent, indent_level=0):
    desc, olindex = [], 0
    indent = ' ' * (indent_level * 4)

    for elem in (e for e in parent if isinstance(e, lxml.html.HtmlElement)):
        if elem.tag == 'p':
            desc.append(elem.text_content().strip())
        elif elem.tag == 'ul':
            desc.extend(_process_book_description(elem.xpath('child::*'),
                                                  indent_level + 1))
        elif elem.tag == 'li':
            desc.append("%s* %s" % (indent, elem.text_content().strip()))
        elif elem.tag == 'ol':
            olindex += 1
            text = elem.text_content().strip()
            desc.append("%s%d- %s" % (indent, olindex, text))

    return desc if indent_level > 0 else '\n'.join(desc)


def _extract_book_download_url(tree):
    hrefs = tree.xpath('//span[@class="download-links"]/a/@href')
    for ref in hrefs:
        if ref.endswith(BOOKS_EXTENSIONS):
            return ref
