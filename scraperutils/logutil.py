# -*- coding: utf-8 -*-

from multiprocessing import RLock


class MultiProcessLogger():

    def __init__(self, logger):
        """
        :param logger: Logger that will be protected by this class.
        :type logger: logging.Logger
        """
        self._logger = logger
        self._lock = RLock()

    def __enter__(self):
        self._lock.acquire()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._lock.release()

    def info(self, msg, *args, **kwargs):
        with self._lock:
            self._logger.info(msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        with self._lock:
            self._logger.warning(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        with self._lock:
            self._logger.error(msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        with self._lock:
            self._logger.critical(msg, *args, **kwargs)

    def log(self, level, msg, *args, **kwargs):
        with self._lock:
            self._logger.log(level, msg, *args, **kwargs)
