#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import os, os.path
import signal
import sys

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType

from scraperutils import netutil
from scraperutils.aitb import AITBScraper, AITBDataBase, RequestError


# ------------------------------------------------------------------------------

__author__ = 'Josep Pon Farreny'
__email__ = 'ponpepo@gmail.com'
__version__ = '0.1'

# ------------------------------------------------------------------------------


# ------------------------------------
#     Module Constants & Variables
# ------------------------------------

VERSION_STR = "All IT Books SCrapper "

_logger = logging.getLogger(__name__)


# ------------
#     Main
# ------------

def main():
    opts = parse_arguments(sys.argv[1:])

    setup_signals()
    setup_logging(opts)

    create_directory_or_exit(get_books_directory(opts))
    user_check_public_ip_or_exit(opts)

    scraper = AITBScraper(opts.max_tries)
    scraper_db = AITBDataBase(get_books_db_path(opts))

    process_aitb_index(opts, scraper, scraper_db)


# -------------------------
#     Scraping Routines
# -------------------------

def process_aitb_index(opts, scraper, scraper_db):
    index = opts.start_index

    while opts.end_index < 0 or index < opts.end_index:
        try:
            _logger.info("Processing index page %d", index)
            process_aitb_index_page(opts, scraper, scraper_db, index)
            index += 1
        except RequestError as e:
            _logger.error("Cannot load index page %d: %e", index, str(e))
            if opts.end_index is None:
                _logger.info("No end index specified exiting at %d ...", index)


def process_aitb_index_page(opts, scraper, scraper_db, index):
    for i in range(opts.max_tries):
        try:
            urls = scraper.get_index_page_book_urls(index)
            for u in urls:
                process_aitb_book(opts, scraper, scraper_db, u)
        except Exception as e:
            if i == opts.max_tries - 1:
                raise e


def process_aitb_book(opts, scraper, scraper_db, book_url):
    book_info, download_url = scraper.get_book_info(book_url)
    fpath = os.path.join(get_books_directory(opts), book_info.file_name)

    if opts.overwrite or not os.path.exists(fpath):
        data = scraper.download_book(download_url)
        with open(fpath, 'wb') as f:
            f.write(data)
            scraper_db.insert_book(book_info)
        _logger.info("[DOWNLOADED] %s (%s)",
                     book_info.file_name, humansize(len(data)))
    elif os.path.exists(fpath):
        _logger.info("[IGNORED] %s (Already Exists)", book_info.file_name)


# ----------------------------------
#     User Interaction Functions
# ----------------------------------

def user_check_public_ip_or_exit(opts):
    print("Requesting public IP, please wait ...")
    public_ip = netutil.get_pulic_ip()

    print("The scraper's public IP is:", public_ip)

    if not opts.accept_ip:
        s = request_user_input("--> Is it correct [y/n]: ",
                               ['y', 'n'], lambda v: v.lower())
        if s == 'n':
            print("Try using a proxy ...")
            sys.exit(1)


def request_user_input(promp="", choices=None, transform_fn=None):
    if not isinstance(choices, (set, frozenset)):
        choices = frozenset(choices)

    def request_input():
        return transform_fn(input(promp)) if transform_fn else input(promp)

    answer = None
    while answer is None or (choices is not None and answer not in choices):
        answer = request_input()
    return answer


# ---------------------------------
#     Generic Utility Functions
# ---------------------------------

def parse_arguments(args):
    """Parses the give arguments

    :param args: The list of arguments to parse.
    """

    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter,
                            description="--- DESC HERE ---",
                            epilog="--- EPILOG ---")

    parser.add_argument('-aip', '--accept_ip', action='store_true',
                        help="Automatically accept the public IP question.")

    parser.add_argument('-d', '--directory', type=str, default='aitb',
                        help="Directory where files will be created/downloaded")

    parser.add_argument('-j', '--jobs', type=int, default=1,
                        help="Maximum number of parallel tasks")

    parser.add_argument('-l', '--log_file', type=FileType('a'),
                        default=sys.stdout, help="Log file")

    parser.add_argument('-o', '--overwrite', action='store_true',
                        help="If set overwrites already downloaded books.")

    parser.add_argument('-s', '--start_index', type=int, default=1,
                        help="Index page from which the scrapping will start")

    parser.add_argument('-e', '--end_index', type=int, default=-1,
                        help="Last index page, if it is negative the process "
                             "will stop at the first error")

    parser.add_argument('-t', '--max_tries', type=int, default=1,
                        help="Maximum number of tries after considering that "
                             "a request has failed")

    parser.add_argument('--verbose', type=int, choices=[1, 2, 3, 4, 5],
                        default=4,
                        help="Verbosity level, Options: 1 - Critical, "
                             "2 - Error, 3 - Warning, 4 - Info and 5 - Debug")

    parser.add_argument('-v', '--version', action='version',
                        version=VERSION_STR)

    return parser.parse_args(args)


def setup_signals():
    def _sigint_sigterm_handler(signal, frame):
        print("Signal aborting")
        sys.exit(-1)

    signal.signal(signal.SIGINT, _sigint_sigterm_handler)


def setup_logging(opts):
    level_map = {1: logging.CRITICAL, 2: logging.ERROR, 3: logging.WARNING,
                 4: logging.INFO, 5: logging.CRITICAL}
    level = level_map[opts.verbose]

    # msg_fmt = '[%(levelname)s - %(name)s - %(asctime)s] %(message)s'
    msg_fmt = '[%(asctime)s - %(levelname)s] %(message)s'
    date_fmt = '%d/%m/%Y %H:%M:%S'
    logging.basicConfig(level=level, format=msg_fmt, datefmt=date_fmt,
                        stream=opts.log_file)

    # set requests package logging level
    logging.getLogger("requests").setLevel(logging.WARNING)


def get_books_directory(opts):
    return os.path.join(opts.directory, 'books')


def get_books_db_path(opts):
    return os.path.join(opts.directory, 'books.db')


def create_directory_or_exit(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError as e:
        _logger.info("Path %s already exists", dir_path)

    if not os.path.isdir(dir_path):
        _logger.critical("%s is not a directory", dir_path)
        sys.exit(-1)


size_suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
def humansize(nbytes):
    if nbytes == 0: return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(size_suffixes) - 1:
        nbytes /= 1024.0
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return "%s %s" % (f, size_suffixes[i])


# -------------------
#     Entry Point
# -------------------

if __name__ == '__main__':
    main()